# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 1.2.2 - 2020-02-12

* Lazy import `django.contrib.gis.db.models.fields.PointField` to avoid GEOS dependency.

## 1.2.1 - 2019-11-23

### Fixed

* Fix bug related to `django.contrib.gis.db.models.fields.PointField`.

## 1.2.0 - 2019-11-23

### Added

* Added support for `django.contrib.gis.db.models.fields.PointField`.
